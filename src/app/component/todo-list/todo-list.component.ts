// todo-list.component.ts
import { Component } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';

@Component({
    selector: 'app-todo-list',
    templateUrl: './todo-list.component.html',
    styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {

    // 用这种依赖注入的方式引入服务
    constructor(private storage: StorageService) {

    }

    public todo: any = '' //在input栏，即将加入todoList
    public todoList = [] as any;
    public doingList = [] as any;
    public doneList = [] as any;

    // 生命周期-初始化
    ngOnInit() {
        this.initTodo()
    }

    // 初始化
    initTodo() {
        let todoArr = this.storage.getItem('todoList');
        if (todoArr) {
            this.todoList = todoArr
        }

        let doingArr = this.storage.getItem('doingList');
        if (doingArr) {
            this.doingList = doingArr
        }

        let doneArr = this.storage.getItem('doneList');
        if (doneArr) {
            this.doneList = doneArr
        }
    }

    // 添加代办时间到todo
    addTodo(e: any) {
        // 回车
        if (e.keyCode == 13) {
            this.todoList.push(this.todo)
            this.todo = ''
            this.storage.setItem('todoList', this.todoList)
        }
    }

    // todo 改为doing
    todoChange(key: any) {
        this.doingList.push(this.todoList[key])
        this.todoList.splice(key, 1)
        this.storage.setItem('todoList', this.todoList)
        this.storage.setItem('doingList', this.doingList)
    }

    // doing 改为done
    doingChange(key: any) {
        this.doneList.push(this.doingList[key])
        this.doingList.splice(key, 1)
        this.storage.setItem('doneList', this.doneList)
        this.storage.setItem('doingList', this.doingList)
    }

    // done 改为todo
    doneChange(key: any) {
        this.todoList.push(this.doneList[key])
        this.doneList.splice(key, 1)
        this.storage.setItem('todoList', this.todoList)
        this.storage.setItem('doneList', this.doneList)
    }

    clearList() {
        this.todoList = []
        this.doingList = []
        this.doneList = []
        localStorage.clear()
    }
}
