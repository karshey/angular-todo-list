// storage.service.ts
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    constructor() { }

    setItem(key: any, value: any) {
        localStorage.setItem(key, JSON.stringify(value));
    }
    getItem(key: any) {
        return JSON.parse(localStorage.getItem(key) || '')
    }
}
